Source: mgetty
Section: comm
Priority: optional
Maintainer: Andreas Barth <aba@ayous.org>
Build-Depends: debhelper (>= 7), texi2html, texinfo, texlive, groff, libx11-dev, patch, perl, dpatch, po-debconf
Standards-Version: 3.8.0
Homepage: http://mgetty.greenie.net/

Package: mgetty
Suggests: mgetty-fax 
Architecture: any 
Depends: ${shlibs:Depends}, ${misc:Depends}, logrotate (>= 3.5.4-1), dpkg (>= 1.15.4) | install-info
Conflicts: mgetty-docs (<= 1.1.30-6)
Replaces: mgetty-docs
Description: Smart Modem getty replacement
 Mgetty is a versatile program to handle all aspects of a modem under Unix.  
 .
 The program 'mgetty' allows you to use a modem for handling external
 logins, receiving faxes and using the modem as a answering machine without
 interfering with outgoing calls.
 .
 This package includes basic modem data capabilities. Install mgetty-fax to 
 get the additional functionality for fax. Install mgetty-voice to get the
 functionality to operate voice modems.
 .  
 Mgetty is also configurable to select programs other than login for special
 connections (eg: uucico, fido or other programs) depending on the login
 userid. It also supports caller-id if the modem and phone line supply it,
 and can deny connections based on originating telephone number.

Package: mgetty-fax
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, cron, mgetty (= ${binary:Version}), perl
Suggests: debianutils (>= 1.6), netpbm, ghostscript, mgetty-viewfax (>= ${binary:Version})
Replaces: mgetty
Conflicts: mgetty (<= 1.1.30-9)
Description: Faxing tools for mgetty
 The fax subsystem of the mgetty package.
 .
 This subsystems has the enhancements that are needed to send and receive
 faxes with mgetty and a Class 2 faxmodem.
 .
 The program 'mgetty' allows you to use a fax modem for receiving faxes 
 and handling external logins without interfering with outgoing calls.

Package: mgetty-viewfax
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Program for displaying Group-3 Fax files under X
 The program 'viewfax' allows you to view Group-3 fax files in an X window.
 Group-3 is the "standard" format for faxes.

Package: mgetty-voice
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, mgetty (= ${binary:Version})
Suggests: mgetty-pvftools
Description: Voicemail handler for mgetty
 Vgetty allows you to add answering machine / voicemail capability to all 
 the other normal mgetty functions.
 .
 The program 'mgetty' allows you to use a voice modem as an answering
 machine and handling external logins without interfering with outgoing
 calls.
 .
 To create and manipulate voice messages, you need the mgetty-pvftools
 package.

Package: mgetty-pvftools
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Conflicts: mgetty-voice (<= 1.1.27-3), ${misc:Depends}
Description: Programs for listening and manipulating pvf and rmd files
 This package contains different utilities from the vgetty package which
 permit to create, listen and manipulate rmd or pvf files. These files are
 the recorded "message files" for using a voice modem as an answering
 machine.
 .
 You will want to install this package to create your greeting messages.

Package: mgetty-docs
Section: doc
Architecture: all
Depends: ${misc:Depends}
Description: Documentation Package for mgetty
 Sample files and lots of documentation for mgetty. Additional source code
 for programs included that allow to set up a user interface for mgetty.
