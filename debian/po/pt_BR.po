# mgetty Brazilian Portuguese translation
# Copyright (C) 2011 THE mgetty's PACKAGE COPYRIGHT HOLDER
# This file is distributed under the same license as the mgetty package.
# Felipe Duarte <mr.fd1979@rocketmail.com>, 2011.
# Eder L. Marques <eder@edermarques.net>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: mgetty\n"
"Report-Msgid-Bugs-To: aba@not.so.argh.org\n"
"POT-Creation-Date: 2007-08-13 10:42+0000\n"
"PO-Revision-Date: 2015-08-13 10:25-0300\n"
"Last-Translator: Eder L. Marques <eder@edermarques.net>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid "Run faxrunqd during system startup?"
msgstr "Executar faxrunqd durante a inicialização do sistema?"

#. Type: boolean
#. Description
#: ../mgetty-fax.templates.master:1001
msgid ""
"The mgetty-fax package contains a daemon (\"faxrunqd\") that can "
"automatically take in charge the sending of the faxes spooled with faxspool. "
"This daemon and the faxrunq utility need /etc/mgetty/faxrunq.config to be "
"configured appropriately in order to run. If you plan to use faxrunqd, "
"please indicate so."
msgstr ""
"O pacote mgetty-fax contém um daemon (\"faxrunqd\") que pode automaticamente "
"ter no comando o envio de faxes enfileirados com o faxspool. Este daemon e o "
"utilitário faxrunq precisam que o arquivo /etc/mgetty/faxrunq.config seja "
"configurado apropriadamente para para que possam ser executados. Se você "
"planeja usar faxrunqd, por favor indique isso."
