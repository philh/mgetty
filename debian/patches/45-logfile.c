#! /bin/sh -e
## 
## All lines beginning with `## DP:' are a description of the patch.
## DP: No description. 

[ -f debian/patches/00patch-opts ] && . debian/patches/00patch-opts
patch_opts="${patch_opts:--f --no-backup-if-mismatch}"

if [ $# -ne 1 ]; then
    echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
    exit 1
fi  
case "$1" in
       -patch) patch $patch_opts -p1 < $0;;
       -unpatch) patch $patch_opts -p1 -R < $0;;
        *)
                echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
		exit 1;;
esac            

exit 0
@DPATCH@
--- mgetty-1.1.30.orig/logfile.c	2002-11-25 08:08:26.000000000 -0500
+++ mgetty-1.1.30/logfile.c	2003-04-28 00:08:35.000000000 -0400
@@ -56,10 +56,16 @@
 /* Most systems have these variables but do not declare them. On many
    of those systems that _do_ declare them, it won't hurt */
 
+/* commented in by was@debian.org on Wed, 27 Nov 2002 01:15:11 -0500
+   because sys_nerr and sys_errlist are deprecated. strerror() is used
+   instead below.*/
+
+#if 0
 #if !defined(__NetBSD__) && !defined( __FreeBSD__ ) && !defined(__OpenBSD__) && !defined(__GLIBC__) && !defined(__MACH__)
 extern int sys_nerr;
 extern char *sys_errlist[];
 #endif
+#endif
 
 /* Interactive Unix is a little bit braindead - does not have atexit(),
  */
@@ -209,6 +213,7 @@
 va_list pvar;
 int     errnr;
 char * p;
+char *error_string;
 static int first_open = TRUE;
 
     if ( level > log_level )	/* log level high enough? */
@@ -325,12 +330,20 @@
     }
     else		/* ERROR or FATAL */
     {
+        error_string = strerror (errnr);
+	if ( error_string == NULL )
+	{
+            if ( errno == EINVAL )
+	        error_string = "<error not in list>";
+	    else
+	        error_string = "<error calling strerror()>";
+	}
+
 	fprintf(log_fp, "\n%02d/%02d %02d:%02d:%02d %s %s: %s",
 		             tm->tm_mon+1,  tm->tm_mday,
 			     tm->tm_hour, tm->tm_min, tm->tm_sec,
 		             log_infix, ws,
-			     ( errnr <= sys_nerr ) ? sys_errlist[errnr]:
-			     "<error not in list>" );
+			     strerror (errnr));
 #ifdef SYSLOG
 	syslog( level == L_FATAL? LOG_ALERT: LOG_ERR, "%s: %m", ws );
 #endif
