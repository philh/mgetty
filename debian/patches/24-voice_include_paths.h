#! /bin/sh -e
## 
## All lines beginning with `## DP:' are a description of the patch.
## DP: No description. 

[ -f debian/patches/00patch-opts ] && . debian/patches/00patch-opts
patch_opts="${patch_opts:--f --no-backup-if-mismatch}"

if [ $# -ne 1 ]; then
    echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
    exit 1
fi  
case "$1" in
       -patch) patch $patch_opts -p1 < $0;;
       -unpatch) patch $patch_opts -p1 -R < $0;;
        *)
                echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
		exit 1;;
esac            

exit 0
@DPATCH@
--- mgetty-1.1.27.orig/voice/include/paths.h
+++ mgetty-1.1.27/voice/include/paths.h
@@ -19,10 +19,10 @@
  * the device name.
  */
 
-#define VGETTY_LOG_PATH "/var/log/vgetty.%s"
+#define VGETTY_LOG_PATH "/var/log/mgetty/vg_%s.log"
 
 /*
  * Filename of the logfile for vm.
  */
 
-#define VM_LOG_PATH "/var/log/vm.log"
+#define VM_LOG_PATH "/var/log/mgetty/vm.log"
