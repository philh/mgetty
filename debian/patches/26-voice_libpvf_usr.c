#! /bin/sh -e
## 
## All lines beginning with `## DP:' are a description of the patch.
## DP: No description. 

[ -f debian/patches/00patch-opts ] && . debian/patches/00patch-opts
patch_opts="${patch_opts:--f --no-backup-if-mismatch}"

if [ $# -ne 1 ]; then
    echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
    exit 1
fi  
case "$1" in
       -patch) patch $patch_opts -p1 < $0;;
       -unpatch) patch $patch_opts -p1 -R < $0;;
        *)
                echo >&2 "`basename $0`: script expects -patch|-unpatch as argument"
		exit 1;;
esac            

exit 0
@DPATCH@
--- mgetty-1.1.27.orig/voice/libpvf/usr.c
+++ mgetty-1.1.27/voice/libpvf/usr.c
@@ -103,9 +103,13 @@
          }
        }
        gsm_encode(r, s, d);
+#if defined(old_USR_GSM_with_head_and_tail_bytes)
        fwrite((char *)gsm_head, 2, 1, fd_out);
+#endif
        fwrite((char *)d, sizeof(d), 1, fd_out);
+#if defined(old_USR_GSM_with_head_and_tail_bytes)
        fwrite((char *)gsm_tail, 3, 1, fd_out);
+#endif
      }
      gsm_destroy(r);
      return(OK);
